from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from .models import Producto, Sucursal, Oferta, Vendedor, Venta
from django.contrib.auth.models import User
from .forms import ContactoForm, ProductoForm, CustomUserCreationForm, SucursalForm, OfertaForm, VendedorForm, VentaForm
from django.contrib import messages
from django.contrib.auth import authenticate, login
from rest_framework import viewsets
from .serializers import ProductoSerializer, SucursalSerializer, VendedorSerializer, VentaSerializer, OfertaSerializer

# Create your views here.


class ProductoViewset(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class SucursalViewset(viewsets.ModelViewSet):
    queryset = Sucursal.objects.all()
    serializer_class = SucursalSerializer


class VendedorViewset(viewsets.ModelViewSet):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer


class VentaViewset(viewsets.ModelViewSet):
    queryset = Venta.objects.all()
    serializer_class = VentaSerializer


class OfertaViewset(viewsets.ModelViewSet):
    queryset = Oferta.objects.all()
    serializer_class = OfertaSerializer


def home(request):
    productos = Producto.objects.all()
    data = {
        'productos': productos
    }
    return render(request, 'app/home.html', data)


def galeria(request):
    return render(request, 'app/galeria.html')


def contacto(request):
    data = {
        'form': ContactoForm()
    }

    if request.method == 'POST':
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Gracias por su contacto"
        else:
            data["form"] = formulario

    return render(request, 'app/contacto.html', data)


@login_required
@staff_member_required
def agregar_producto(request):
    data = {
        'form': ProductoForm()
    }

    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, "El Producto Ha Sido Registrado")
        else:
            data["form"] = formulario

    return render(request, 'venta/agregar.html', data)


@login_required
def listar_producto(request):
    productos = Producto.objects.all()

    data = {
        'productos': productos
    }
    return render(request, 'venta/listar.html', data)


@login_required
@staff_member_required
def modificar_producto(request, id):

    producto = get_object_or_404(Producto, id=id)

    data = {
        'form': ProductoForm(instance=producto)
    }

    if request.method == 'POST':
        formulario = ProductoForm(
            data=request.POST, files=request.FILES, instance=producto)
        if formulario.is_valid():
            formulario.save()
            messages.success(
                request, "El Producto Se Ha Modificado Correctamente")
            return redirect(to="listar_producto")
        else:
            data["form"] = formulario

    return render(request, 'venta/modificar.html', data)


@login_required
@staff_member_required
def eliminar_producto(request, id):
    producto = get_object_or_404(Producto, id=id)
    producto.delete()
    messages.success(request, "El Producto Se Ha Eliminado Correctamente")
    return redirect(to="listar_producto")


@login_required
@staff_member_required
def registro(request):
    data = {
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(
                username=formulario.cleaned_data["username"], password=formulario.cleaned_data["password1"])
            login(request, user)
            messages.success(request, "Te Has Registrado Correctamente")
            return redirect(to="home")

    else:
        form = VendedorForm()
    return render(request, "venta/crearvendedor.html", {"titulo": "Registrar un vendedor", "form": form})


def listar_vendedor(request):
    vendedores = Vendedor.objects.all()

    data = {
        'vendedores': vendedores
    }
    return render(request, 'venta/listarvendedor.html', data)


def modificar_vendedor(request, pk):

    vendedor = Vendedor.objects.get(id=pk)

    data = {
        'form': VendedorForm(instance=vendedor)
    }

    if request.method == 'POST':
        formulario = VendedorForm(
            data=request.POST, files=request.FILES, instance=vendedor)
        if formulario.is_valid():
            formulario.save()
            messages.success(
                request, "El Vendedor Se Ha Modificado Correctamente")
            return redirect(to="listar_vendedor")
        else:
            data["form"] = formulario
    return render(request, 'registration/registro.html', data)


@login_required
@staff_member_required
def crear_tienda(request):
    if request.method == "POST":
        form = SucursalForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Sucursal.objects.create(nombre=data.get("nombre"), ciudad=data.get("ciudad"), comuna=data.get(
                "comuna"), direccion=data.get("direccion"), telefono=data.get("telefono"), correo=data.get("correo"))
            messages.success(
                request, "La tienda Se Ha Registrado Correctamente")
        return redirect(to="home")
    else:
        form = SucursalForm()
    return render(request, "venta/creartienda.html",  {"titulo": "Registrar una sucursal", "form": form})


@login_required
def listar_tienda(request):
    sucursales = Sucursal.objects.all()

    data = {
        'sucursales': sucursales
    }
    return render(request, 'venta/listartienda.html', data)


@login_required
@staff_member_required
def modificar_tienda(request, id):

    sucursal = get_object_or_404(Sucursal, id=id)

    data = {
        'form': SucursalForm(instance=sucursal)
    }

    if request.method == 'POST':
        formulario = SucursalForm(
            data=request.POST, files=request.FILES, instance=sucursal)
        if formulario.is_valid():
            formulario.save()
            messages.success(
                request, "La Sucursal Se Ha Modificado Correctamente")
            return redirect(to="listar_tienda")
        else:
            data["form"] = formulario

    return render(request, 'venta/modificartienda.html', data)


@login_required
@staff_member_required
def eliminar_tienda(request, pk):
    sucursal = Sucursal.objects.get(id=pk)
    sucursal.delete()
    messages.success(request, "La Sucursal Se Ha Eliminado Correctamente")
    return redirect(to="listar_tienda")


@login_required
@staff_member_required
def crear_oferta(request):
    if request.method == "POST":
        form = OfertaForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Oferta.objects.create(fechaFin=data.get("fechaFin"), porcentaje=int(data.get("porcentaje")) / 100, producto=data.get(
                "producto"), sucursal=data.get("sucursal"), fechaInicio=data.get("fechaInicio"), vigente=data.get("vigente"))
            messages.success(request, "La Oferta Ha Sido Registrado")
    else:
        form = OfertaForm()
    return render(request, "venta/crearoferta.html", {"titulo": "Registrar una oferta", "form": form})


@login_required
def listar_oferta(request):
    ofertas = Oferta.objects.all()

    data = {
        'ofertas': ofertas
    }
    return render(request, 'venta/listaroferta.html', data)

@login_required
@staff_member_required
def eliminar_oferta(request, pk):
    try:
        oferta = Oferta.objects.get(codigo=pk)
        if oferta:
            oferta.delete()
        messages.success(request, "La tienda Ha Sido Eliminada")
    except ObjectDoesNotExist:
        return render(request, "venta/listaroferta.html", {"titulo": "Error al eliminar oferta"})

@login_required
@staff_member_required
def crear_vendedor(request):
    if request.method == "POST":
        form = VendedorForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Vendedor.objects.create(nombres=data.get("nombres"), run=data.get(
                "run"), apPaterno=data.get("apPaterno"), apMaterno=data.get("apMaterno"))
            messages.success(request, "El Vendedor Ha Sido Registrado")
            return redirect(to="home")

    else:
        form = VendedorForm()
    return render(request, "venta/crearvendedor.html", {"titulo": "Registrar un vendedor", "form": form})

@login_required
def listar_vendedor(request):
    vendedores = Vendedor.objects.all()

    data = {
        'vendedores': vendedores
    }
    return render(request, 'venta/listarvendedor.html', data)

@login_required
@staff_member_required
def modificar_vendedor(request, pk):

    vendedor = Vendedor.objects.get(id=pk)

    data = {
        'form': VendedorForm(instance=vendedor)
    }

    if request.method == 'POST':
        formulario = VendedorForm(
            data=request.POST, files=request.FILES, instance=vendedor)
        if formulario.is_valid():
            formulario.save()
            messages.success(
                request, "El Vendedor Se Ha Modificado Correctamente")
            return redirect(to="listar_vendedor")
        else:
            data["form"] = formulario
    return render(request, "venta/modificarvendedor.html", data)

@login_required
@staff_member_required
def eliminar_vendedor(request, pk):
    vendedor = Vendedor.objects.get(id=pk)
    vendedor.delete()
    messages.success(request, "El Vendedor Se Ha Eliminado Correctamente")
    return redirect(to="listar_vendedor")

@login_required
@staff_member_required
def modificar_oferta(request, pk):
    try:
        oferta = Oferta.objects.get(id=pk)
        if request.method == "POST":
            form = OfertaForm(request.POST)
            if form.is_valid():
                data = form.cleaned_data
                oferta.fechaFin = data.get("fechaFin")
                oferta.porcentaje = data.get("porcentaje")
                oferta.producto = data.get("producto")
                oferta.sucursal = data.get("sucursal")
                oferta.vigente = data.get("vigente")
                oferta.save()
                return redirect(to="listar_oferta")
        else:
            form = OfertaForm({"fechaFin": oferta.fechaFin, "porcentaje": oferta.porcentaje,
                               "producto": oferta.producto, "sucursal": oferta.sucursal, "vigente": oferta.vigente})
    except ObjectDoesNotExist:
        oferta = None
        form = None
    return render(request, "venta/modificaroferta.html", {"titulo": "Actualizar oferta", "form": form, "oferta": oferta})

@login_required
def registrar_venta(request):
    if request.method == "POST":
        form = VentaForm(request.POST)
        vendedor = Vendedor.objects.get(usuario=request.user)
        if form.is_valid():
            data = form.cleaned_data
            Venta.objects.create(vendedor=vendedor, sucursal=vendedor.sucursal, producto=data.get(
                "producto"), cantidad=data.get("cantidad"), comentario=data.get("comentario"))  # Se registra una nueva venta
            return redirect("modulo_ventas")
    else:
        form = VentaForm()
    # Retorna la vista solicitada
    return render(request, "venta/registrarventa.html", {"titulo": "Registrar una venta", "form": form})


# def mostrar_venta(request, pk):
#	try:
#		venta = Venta.objects.get(codigo=pk)
#	except:
#		venta = None
 #   return redirect(request, "venta/mostrarventa.html", { "venta": venta }) 

@login_required
def mostrar_venta(request):
    ventas = Venta.objects.all()

    data = {
        'ventas': ventas
    }
    return render(request, 'venta/mostrarventa.html', data)
