from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Marca(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

class Producto(models.Model):
    nombre = models.CharField(max_length=50)
    precio = models.IntegerField()
    descripcion = models.TextField()
    nuevo = models.BooleanField()
    marca = models.ForeignKey(Marca, on_delete=models.PROTECT)
    fecha_fabricacion = models.DateField()
    imagen = models.ImageField(upload_to="productos",null=True)

    def __str__(self):
        return  self.nombre

opciones_consultas = [
    [0, "consulta"],
    [1, "reclamo"],
    [2, "sugerencia"],
    [3, "felicitaciones"]
]


class Contacto(models.Model):
    nombre = models.CharField(max_length=50)
    correo = models.EmailField()
    tipo_consulta = models.IntegerField(choices=opciones_consultas)
    mensaje = models.TextField()
    avisos = models.BooleanField()

    def __str__(self):
        return self.nombre


class Sucursal(models.Model):
    nombre = models.CharField(max_length = 30)
    direccion = models.CharField(max_length = 50)
    ciudad = models.CharField(max_length = 30)
    comuna = models.CharField(max_length = 30)
    telefono = models.CharField(max_length = 10)
    correo = models.EmailField()

    def __str__(self):
	    return self.nombre

class Oferta(models.Model):
	fechaInicio = models.DateField(auto_now_add = True)
	fechaFin = models.DateField()
	vigente = models.BooleanField(default = True)
	porcentaje = models.DecimalField(max_digits = 3, decimal_places = 2)
	producto = models.ForeignKey(Producto, on_delete = models.CASCADE)
	sucursal = models.ForeignKey(Sucursal, on_delete = models.CASCADE)

class Vendedor(models.Model):
    
    nombres = models.CharField(max_length = 30)
    run = models.CharField(unique = True, max_length = 10)
    apPaterno = models.CharField(max_length = 20)
    apMaterno = models.CharField(max_length = 20)
    encargado = models.BooleanField(default = True)


class Venta(models.Model):
	
	vendedor = models.ForeignKey(Vendedor, on_delete = models.DO_NOTHING)
	sucursal = models.ForeignKey(Sucursal, on_delete = models.DO_NOTHING)
	fechaHora = models.DateTimeField(auto_now_add = True)
	producto = models.ForeignKey(Producto, on_delete = models.DO_NOTHING)
	cantidad = models.IntegerField()
	comentario = models.TextField(null = True)
	anulada = models.BooleanField(default = False)
