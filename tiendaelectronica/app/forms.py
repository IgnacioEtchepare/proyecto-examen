from django import forms
from .models import Contacto, Producto , Sucursal , Oferta , Vendedor, Venta

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class ContactoForm(forms.ModelForm):

    class Meta:
        model = Contacto
        fields = '__all__'


class ProductoForm(forms.ModelForm):

    class Meta:
        model = Producto
        fields = '__all__'

class CustomUserCreationForm (UserCreationForm):
    
    class Meta:
        model = User
        fields = ['username','first_name','last_name','email','password1','password2']


class SucursalForm(forms.ModelForm):

    class Meta:
        model = Sucursal
        fields = '__all__'


class OfertaForm(forms.ModelForm):

    class Meta:
        model = Oferta
        fields = '__all__'


class VendedorForm(forms.ModelForm):

    class Meta:
        model = Vendedor
        fields = '__all__'

class VentaForm(forms.ModelForm):

    class Meta:
        model = Venta
        fields = '__all__'
