from django.contrib import admin
from django.urls import path, include
from .views import home, galeria, contacto, agregar_producto, listar_producto, modificar_producto, eliminar_producto, registro, ProductoViewset, crear_tienda, crear_oferta, listar_tienda, modificar_tienda, eliminar_tienda, crear_vendedor, listar_vendedor, modificar_vendedor, eliminar_vendedor, registrar_venta, mostrar_venta, modificar_oferta, eliminar_oferta, listar_oferta, SucursalViewset, VendedorViewset, VentaViewset, OfertaViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('producto', ProductoViewset)
router.register('sucursal', SucursalViewset)
router.register('vendedor', VendedorViewset)
router.register('venta', VentaViewset)
router.register('ofera', OfertaViewset)

urlpatterns = [
    path('', home, name="home"),
    path('galeria/', galeria, name="galeria"),  
    path('contacto/', contacto, name="contacto"),
    path('agregar-producto/', agregar_producto, name="agregar_producto"),
    path('listar-producto/', listar_producto, name="listar_producto"),
    path('modificar-producto/<id>/', modificar_producto, name="modificar_producto"),
    path('eliminar-producto/<id>/', eliminar_producto, name="eliminar_producto"),
    path('registro/', registro, name="registro"),
    path('api/', include(router.urls)),
    path('crear-tienda/', crear_tienda, name="crear_tienda"),
    path('crear-oferta/', crear_oferta, name="crear_oferta"),
    path('listar-tienda/', listar_tienda, name="listar_tienda"),
    path('modificar-tienda/<id>/', modificar_tienda, name="modificar_tienda"),
    path('eliminar-tienda/<pk>/', eliminar_tienda, name="eliminar-tienda"),
    path('crear-vendedor/', crear_vendedor, name="crear_vendedor"),
    path('listar-vendedor/', listar_vendedor, name="listar_vendedor"),
    path('modificar-vendedor/<pk>/', modificar_vendedor, name="modificar_vendedor"),
    path('eliminar-vendedor/<pk>/', eliminar_vendedor, name="eliminar_vendedor"),
    path('registrar-venta/', registrar_venta, name="registrar_venta"),
    path('mostrar-venta/', mostrar_venta, name="mostrar_venta"),
    path('modificar-oferta/<pk>/', modificar_oferta, name="modificar_oferta"),
    path('eliminar-oferta/<pk>/', eliminar_oferta, name="eliminar_oferta"),
    path('listar-oferta/', listar_oferta, name="listar_oferta"),
]
