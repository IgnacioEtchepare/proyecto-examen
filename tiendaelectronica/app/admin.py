from django.contrib import admin
from .models import Marca, Producto , Contacto , Sucursal , Oferta ,Vendedor


class ProductoAdmin(admin.ModelAdmin):
    list_display = ["nombre","precio","nuevo", "marca"]
    list_editable = [ "precio", "nuevo", "marca"]
    search_fields = ["nombre"]
    list_filter = ["marca","nuevo"]
    

admin.site.register(Marca)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Contacto)
admin.site.register(Sucursal)
admin.site.register(Oferta)
admin.site.register(Vendedor)